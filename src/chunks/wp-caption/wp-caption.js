(function($) {
  $(".wp-caption > a").fancybox({
    caption: function(instance, item) {
      return $(this).closest('.wp-caption').find('figcaption').html();
    }
  });
})(jQuery)
