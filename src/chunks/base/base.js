(function($){
  var $base = $('.base'),
    $track = $base.find('.base__track'),
    $list = $base.find('.base__list'),
    $items = $base.find('.base__item'),
    arrow = {
      $prev: $base.find('.base__arrow_prev'),
      $next: $base.find('.base__arrow_next')
    },
    $toggle = $base.find('.base__toggle'),
    listWidth,
    stepWidth,
    itemWidth,
    translateX = 0,
    minTranslateX;

  function throttle(func, ms) {
    var isThrottled = false,
      savedArgs,
      savedThis;
  
    function wrapper() {
  
      if (isThrottled) { // (2)
        savedArgs = arguments;
        savedThis = this;
        return;
      }
  
      func.apply(this, arguments); // (1)
  
      isThrottled = true;
  
      setTimeout(function() {
        isThrottled = false; // (3)
        if (savedArgs) {
          wrapper.apply(savedThis, savedArgs);
          savedArgs = savedThis = null;
        }
      }, ms);
    }
  
    return wrapper;
  }

  function refresh() {
    itemWidth = $items.first().outerWidth();
    listWidth = itemWidth * $items.length;
    stepWidth = itemWidth * 3;
    minTranslateX = -(listWidth - $list.outerWidth());
  }

  function setTranslateX(newTranslateX) {
    if($base.hasClass('base_open')) return;
    translateX = newTranslateX;
    $list.css('transform', 'translateX(' + newTranslateX + 'px)');
    $base.trigger('change', getStatus(translateX));
  }

  function getStatus(translateX) {
    var status;

    switch(translateX) {
      case 0:
        status = 'start';
        break;
      case minTranslateX:
        status = 'end';
        break;
      default:
        status = 'between'
    }

    return status;
  }

  function next() {
    var newTranslateX = translateX - stepWidth;

    if(newTranslateX >= minTranslateX) {
      setTranslateX(newTranslateX);
    } else if(translateX > minTranslateX) {
      setTranslateX(minTranslateX);
    }
  }

  function prev() {
    var newTranslateX = translateX + stepWidth;

    if(newTranslateX <= 0 ) {
      setTranslateX(newTranslateX);
    } else if(translateX < 0) {
      setTranslateX(0);
    }
  }

  function arrowCheck(e, state) {
    switch(state) {
      case 'start':
        arrow.$prev.addClass('base__arrow_disabled');
        arrow.$next.removeClass('base__arrow_disabled');
        break; 
      case 'end':
        arrow.$next.addClass('base__arrow_disabled');
        arrow.$prev.removeClass('base__arrow_disabled');
        break;
      case 'between':
        arrow.$prev.removeClass('base__arrow_disabled');
        arrow.$next.removeClass('base__arrow_disabled');
        break;
    }
  }
  
  window.next = next;
  window.prev = prev;

  refresh();
  arrowCheck(null, 'start');

  var throttleRefresh = throttle(refresh, 100);

  arrow.$next.click(next);
  arrow.$prev.click(prev);
  $base.on('change', arrowCheck);
  $base.on('swipeRight', prev);
  $base.on('swipeLeft', next);

  $base.on('touchstart', function(e) {
    var startTime = new Date(),
        startPos = {
          x: e.touches[0].clientX,
          y: e.touches[0].clientY
        };

    var timer = setInterval(function() {
      var nowTime = new Date();
      
      if((nowTime - startTime) > 1000) {
        swipeCancel();
      }
    }, 50);

    $base.on('touchmove', swipeDetect);
    $base.on('touchend', swipeCancel);

    function swipeCancel() {
      $base.off('touchmove', swipeDetect);
      $base.off('touchend', swipeCancel);
      clearInterval(timer);
    }

    function swipeDetect(e) {
      var nowPos = {
        x: e.touches[0].clientX,
        y: e.touches[0].clientY
      }

      if((nowPos.x - startPos.x) > 80) {
        $base.trigger('swipeRight');
        swipeCancel();
      }

      if((startPos.x - nowPos.x) > 80) {
        $base.trigger('swipeLeft');
        swipeCancel();
      }
    }
  });

  $(window).resize(function() {
    throttleRefresh();
  });

  // Open / close list

  $toggle.click(function(){
    setTranslateX(0);
    $base.toggleClass('base_open');
  });

  // Mouse whell horizontal scroll

  $list.mousewheel(function(e, delta) {
    if($base.hasClass('base_open')) return;
    delta < 0 ? next() : prev();
    e.preventDefault();
  });

  // Disable go to link, if draged list

  /*
  $list.find('a').mousedown(function(e) {
    const that = this,
          pos = {
            x: e.clientX,
            y: e.clientY
          }

    $(that).one('mouseup', function(e) {
      if(
        Math.abs(e.clientX - pos.x) > 10 ||
        Math.abs(e.clientY - pos.y) > 10
      ) {
        $(that).one('click', function(e) {
          return false;
        });
      }
    });
  });
  */
})(jQuery)
