<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
  <?php while(have_posts()) : the_post(); ?>
    <?php get_template_part('chunks/content', 'single'); ?>

    <?php if(comments_open() || get_comments_number()) : ?>
      <div class="content__comments">
        <?php comments_template(); ?>
      </div>
    <?php endif; ?>
  <?php endwhile; ?>
<?php get_footer(); ?>
