<?php
/**
 * The template part for displaying content
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
  <div class="entry__date">
    <?php the_time('j.m.y'); ?>
  </div>
	<?php the_title( sprintf( '<h2 class="entry__title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	<?php
    $cats = [];
    $tags = [];

    if(!empty(get_the_category())) {
      foreach(get_the_category() as $cat)
        $cats[] = "<a href='/{$cat->category_nicename}'>{$cat->cat_name}</a>";
    }

    if(!empty(get_the_tags())) {
      foreach(get_the_tags() as $tag)
        $tags[] = "<a href='/tag/{$tag->slug}'>{$tag->name}</a>";
    }

    $taxs = array_merge($cats, $tags);
  ?>
  <?php if(!empty($taxs)): ?>
	  <div class="entry__tags"><i class="icon-tag"></i>
      <?php echo join(', ', $taxs); ?>
    </div>
	<?php endif; ?>
	<?php crazyk_post_thumbnail(); ?>
	<div class="entry__content">
		<?php
			the_content('Читать далее');

			wp_link_pages([
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'crazyk' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'crazyk' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			]);
		?>
	</div>
  <div class="entry__author">
    <i class="icon-user"></i> <?php the_author(); ?>
  </div>
	<?php
		edit_post_link(
			'<i class="icon-pencil"></i> Редактировать',
			'<footer class="entry__footer">',
			'</footer>'
		);
	?>
</article><!-- #post-## -->
