<?php
/**
 * The template part for displaying single page
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
	<?php the_title(sprintf('<h1 class="entry__title">', esc_url(get_permalink())), '</h1>'); ?>
	<div class="entry__content">
		<?php
			the_content();

			wp_link_pages([
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'crazyk' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'crazyk' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			]);
		?>
	</div><!-- .entry__content -->
	<?php
		edit_post_link(
			'<i class="icon-pencil"></i> Редактировать',
			'<footer class="entry__footer">',
			'</footer>'
		);
	?>
</article><!-- #post-## -->

