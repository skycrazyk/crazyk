<nav class="nav">
  <div class="base nav__base">
    <div class="base__arrow base__arrow_prev base__arrow_disabled"></div>
    <div class="base__track">
      <ul class="base__list">
        <?php
          $cat_current = get_category(get_query_var('cat'));

          $cats = get_categories([
            'orderby' => 'count',
            'order' => 'DESC'
          ]);

          foreach($cats as $cat) :

            // Count of posts in category

            if(get_query_var('tag')) {
              unset($countPosts);

              $args = [
                'slug' => get_query_var('tag'),
                'fields' => 'ids'
              ];

              $tag_id = get_tags($args)[0];

              $catTags = json_decode(get_field('tax-num-entry', "category_{$cat->term_id}"));

              if(!empty($catTags->$tag_id)) {
                $countPosts = $catTags->$tag_id;
              }
            }

            // Category class modifier

            $mods = [];
            unset($mod);

            if(!is_wp_error($cat_current) && ($cat->slug == $cat_current->slug)) {
              array_push($mods,'base__item_active');
            }

            if(isset($countPosts) && empty($countPosts)) {
              array_push($mods,'base__item_empty');
            }

            if(!empty($mods)) $mod = join(' ', $mods);

            // Category href

            $link = ["/cat/{$cat->slug}"];

            if(get_query_var('tag') && isset($countPosts)) {
              array_push($link, "?tag=" . get_query_var('tag'));
            }

            $link = join('', $link);
        ?>
          <li class="base__item <?php if(isset($mod)) echo $mod; ?>">
            <a
              class="base__link"
              href="<?php echo $link; ?>"
              style="background-image: url(<?php the_field('tag-image', "category_{$cat->term_id}"); ?>)"
            >
              <span class="base__caption"><?php echo $cat->name; ?></span>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
    <div class="base__arrow base__arrow_next"></div>
    <div class="base__toggle"></div>
  </div>

  <div class="container nav__tax">

    <?php if(is_category()) : ?>
      <div class="nav-tax nav-tax_category nav__category">
        <div class="nav-tax__caption">Категория:</div>
        <div class="nav-tax__content">
          <a href="<?php echo join('', ["/cat/", get_category(get_query_var('cat'))->slug]); ?>" title="Текущая категория: <?php echo get_category(get_query_var('cat'))->name; ?>">
            <?php echo get_category(get_query_var('cat'))->name; ?>
          </a>
          <?php
            if(isset($_GET['tag'])) {
              $tags = get_tags(['slug' => get_query_var('tag'), 'fields' => 'names']);
            }

            if(!empty($tags)) echo "&nbsp;/&nbsp;{$tags[0]}";
          ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      $termsSource = get_tags([
        'orderby' => 'count',
        'order' => 'DESC'
      ]);

      $terms = [];

      if(!empty($termsSource) && !is_wp_error($termsSource) && is_category()) :
        $cat_id = get_query_var('cat');

        // Tag filter

        foreach($termsSource as $key => $term) {
          $tagCats = json_decode(get_field('tax-num-entry', "post_tag_{$term->term_id}"));

          if(empty($tagCats->$cat_id)) {
            continue;
          }

          if(isset($_GET['tag']) && $_GET['tag'] == $term->slug) {
            continue;
          }

          array_push($terms, $term);
        }

        if(
          !empty($terms) &&
          ( (1 < count($terms)) ||
            (count($terms) == 1 && isset($_GET['tag']) && $terms[0] !== $_GET['tag'])
          )
        ) :
    ?>
      <div class="nav-tax nav-tax_tag nav__tag">
        <div class="nav-tax__caption">Теги:</div>
        <ul class="tags nav-tax__content">
          <?php foreach($terms as $term) : ?>
            <li class="tags__item <?php if($term->slug == get_query_var('tag')) echo "tags__item_active"; ?>">
              <a href="<?php echo "?tag={$term->slug}"; ?>" class="tags__link" title="Тег &#34;<?php echo $term->name; ?>&#34;">
                <?php echo $term->name; ?>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php endif; endif; ?>
  </div>

  <div class="nav__search">
    <form class="search-form search-form_nav" action="<?php echo esc_url( home_url( '/' ) ); ?>" id="search-form">
      <input type="text" class="search-form__field" placeholder="Поиск ..." name="s" autocomplete="off" value="<?php echo get_search_query(); ?>" />
      <button type="submit" class="button search-form__button"><i class="icon-search"></i></button>
      <ul class="search-result search-form__result">
          <li class="search-result__group search-result__group_category">
            <div class="search-result__caption"><i class="icon-folder-open"></i> Категории</div>
            <ul class="search-result__list"></ul>
          </li>
          <li class="search-result__group search-result__group_tag">
            <div class="search-result__caption"><i class="icon-tags"></i> Теги</div>
            <ul class="search-result__list"></ul>
          </li>
          <li class="search-result__group search-result__group_post">
            <div class="search-result__caption"><i class="icon-doc-text"></i> Статьи</div>
            <ul class="search-result__list"></ul>
          </li>
        </ul>
    </form>
  </div>
</nav>
