<?php
/**
 * The template part for displaying single posts
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
  <div class="entry__date">
    <?php the_time('j.m.y'); ?>
  </div>
	<?php the_title(sprintf('<h1 class="entry__title">', esc_url(get_permalink())), '</h1>'); ?>
	<?php
    $cats = [];
    $tags = [];

    if(get_the_category()) {
      foreach(get_the_category() as $cat)
        $cats[] = "<a href='/cat/{$cat->category_nicename}'>{$cat->cat_name}</a>";
    }

    if(get_the_tags()) {
      foreach(get_the_tags() as $tag)
        $tags[] = "<a href='/tag/{$tag->slug}'>{$tag->name}</a>";
    }

    $taxs = array_merge($cats, $tags);
  ?>
  <?php if(!empty($taxs)): ?>
	  <div class="entry__tags"><i class="icon-tag"></i> <?php echo join(', ', $taxs); ?></div>
	<?php endif; ?>
	<?php crazyk_post_thumbnail(); ?>
	<div class="entry__content">
		<?php
			the_content('Читать далее');

			// foreach(get_the_tags() as $tag)
			  // get_template_part('chunks/meta/meta', $tag->slug);

			wp_link_pages([
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'crazyk' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'crazyk' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			]);
		?>
	</div><!-- .entry__content -->
  <div class="author-meta entry__author-full">
    <div class="author-meta__photo"><?php echo get_avatar(get_the_author_meta('user_email')); ?></div>
    <div class="author-meta__intro">
      <div class="author-meta__title">Автор</div>
      <div class="author-meta__name"><?php the_author_posts_link(); ?></div>
      <div class="author-meta__about"><?php the_author_meta('description'); ?></div>
    </div>
  </div>
	<?php
		edit_post_link(
			'<i class="icon-pencil"></i> Редактировать',
			'<footer class="entry__footer">',
			'</footer>'
		);
	?>
</article><!-- #post-## -->

