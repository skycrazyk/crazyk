<?php

if(!function_exists('crazyk_setup'))
{
  function crazyk_setup()
  {
    add_theme_support('title-tag');

    add_theme_support('post-thumbnails');
    // set_post_thumbnail_size(1200, 1200);

    register_nav_menus(['footermenu' => 'Меню в футере']);

    add_theme_support('html5', [
      //'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ]);
  }
};

add_action( 'after_setup_theme', 'crazyk_setup' );

/**
 * https://github.com/sindresorhus/gulp-rev/blob/master/integration.md
 * @param  string  $filename
 * @return string
 */

function asset_path($filename)
{
  $manifest_path = get_template_directory() . '/build/rev-manifest.json';

  if (file_exists($manifest_path))
    $manifest = json_decode(file_get_contents($manifest_path), TRUE);
  else
    $manifest = [];

  if (array_key_exists($filename, $manifest))
      return $manifest[$filename];

  return $filename;
};

/**
 * Enqueues scripts and styles.
 */
function crazyk_scripts()
{

  // Theme stylesheet.
  wp_enqueue_style('crazyk-style', get_template_directory_uri() . '/build/' . asset_path('style.css'));

  // Theme scripts.
  wp_enqueue_script('crazyk-script', get_template_directory_uri() . '/build/' . asset_path('script0.js'), [], false, true);

  if(is_singular() && comments_open() && get_option('thread_comments'))
    wp_enqueue_script('comment-reply');
}

add_action( 'wp_enqueue_scripts', 'crazyk_scripts' );

/*
* Toolbar to bottom
*/

add_action('admin_bar_init', function(){
  remove_action('wp_head', '_admin_bar_bump_cb');
});

/*
* Remove WP version
*/

remove_action('wp_head', 'wp_generator');
add_filter('the_generator', '__return_empty_string');

/*
* Remove publish from email
*/

add_filter('xmlrpc_enabled', '__return_false');

add_filter('wpseo_breadcrumb_single_link', 'breadcrumbsH1', 10, 1);

/*
* Remove H2 header from pagination
*/

function removeH2HeaderFromPagination( $template, $class )
{
  return '
  <nav class="navigation %1$s" role="navigation">
    <div class="nav-links">%3$s</div>
  </nav>
  ';
}

add_filter('navigation_markup_template', 'removeH2HeaderFromPagination', 10, 2 );

/*
* Read more wrap
*/

function readMoreWrap($more_link)
{
  return "<p class='more-link-wrap'>$more_link</p>";
}

add_filter('the_content_more_link', 'readMoreWrap', 10, 1);

/*
* Facebook SDK
*/

add_action('wp_head', 'facebookSDK');

function facebookSDK()
{
  ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9&appId=453937278292784";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
  <?php
}

/*
* Thumbnail image
*/

function crazyk_post_thumbnail()
{
  if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
    return;
  }

  if ( is_singular() ) :
  ?>

    <div class="entry__thumbnail entry-thumb">
      <?php the_post_thumbnail('medium', array( 'alt' => the_title_attribute('echo=0'), 'class' => 'entry-thumb__img' )); ?>
    </div>

  <?php else : ?>

    <a class="entry__thumbnail entry-thumb" href="<?php the_permalink(); ?>" aria-hidden="true">
      <?php the_post_thumbnail('medium', array( 'alt' => the_title_attribute('echo=0'), 'class' => 'entry-thumb__img' )); ?>
    </a>

  <?php endif;
}

/*
* Add wrapper for embed
*/

add_action('embed_oembed_html', 'add_wrap_for_embed', 10, 3);

function add_wrap_for_embed($return, $url, $attr)
{
  $ratio = isset($attr['ratio']) ? $attr['ratio'] : '16-9';
  $wrap = '<span class="embed-responsive embed-responsive_%1$s"><span class="embed-responsive__inner">%2$s</span></span>';
  return sprintf($wrap, $ratio, $return);
}

/*
* Wrap iframe to responsive markup
*/

function embed_responsive($attr, $shortcode_content = null)
{
  $ratio = isset($attr['ratio']) ? $attr['ratio'] : '16-9';
  $isIframe = preg_match('@^<iframe@', $shortcode_content);

  if(!$isIframe)
  {
    $shortcode_content = sprintf('<iframe src="%1$s" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', $shortcode_content);
  }

  $wrap = '<p><span class="embed-responsive embed-responsive_%1$s"><span class="embed-responsive__inner">%2$s</span></span></p>';
  return sprintf($wrap, $ratio, $shortcode_content);
}

add_shortcode('embedresp', 'embed_responsive');

/*
* Any video
*/

function any_video($content)
{
  global $wp_embed;
  $result = '';
  $blank = [];

  $detect = [
    'iframe' => '@^<iframe(?:[^\>]*(ratio="[^\"]*"))?@',
    'embedrespsc' => '@^\[embedresp@',
    'oembed' => '@^http(?:s)?://(?:www\.)(?:youtube|vimeo)@',
    'oembedsc' => '@^\[embed(?:[^\]]*)\]@',
  ];

  foreach($detect as $type => $regex)
  {
    $test = preg_match($regex, $content, $blank);
    if($test) break;
  }

  if($test)
  {
    switch($type)
    {
      case 'iframe':
        $ratio = isset($blank[1]) ? " {$blank[1]}" : '';
        $result = do_shortcode("[embedresp{$ratio}]{$content}[/embedresp]");
        break;

      case 'oembed':
        $result = $wp_embed->run_shortcode("[embed]{$content}[/embed]");
        break;

      case 'oembedsc':
        $result = $wp_embed->run_shortcode($content);
        break;

      default:
        $result = do_shortcode($content);
        break;
    }
  }
  else
  {
    $result = '<p>Not allowed template of video</p>';
  }

  return $result;
}

/*
* Add editor styles
*/

// add_action('current_screen', 'crazyk_add_editor_styles');

function crazyk_add_editor_styles()
{
  add_editor_style( 'build/editor-styles.css' );
}


/*
* Add yandex metrika and google analytics
*/

add_action('wp_head', 'add_analytics');

function add_analytics()
{
  ?>
  <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter45076766 = new Ya.Metrika({ id:45076766, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/45076766" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
  <!-- Google analytics --> <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');    ga('create', 'UA-101485522-1', 'auto');    ga('send', 'pageview');  </script> <!-- Google analytics -->
  <?php
}

/*
* Remove read more anchor
*/

add_filter('the_content_more_link', 'remove_more_link_scroll');

function remove_more_link_scroll($link)
{
  $link = preg_replace( '|#more-[0-9]+|', '', $link );
  return $link;
}

/*
* Post tag meta
*/

add_filter('the_content', 'postTagMeta', 20);

function postTagMeta($content)
{
  $postMeta = '';
  $tags = get_the_tags();

  if (is_single() && !empty($tags))
  {
    foreach($tags as $tag)
    {
      $config = [];
      $metaFile = dirname(__FILE__) . '/chunks/meta/' . $tag->slug . '.php';

      if(is_file($metaFile))
      {
        $config = include $metaFile;

        foreach($config as $suffix => $func)
        {
          $fieldName = join('-', ['tag', $tag->slug, $suffix]);
          $field = get_field_object($fieldName);

          if(!empty($field['value']))
          {
            $postMeta .= "<h3>{$field['label']}</h3>" . $func($fieldName);
          }
        }
      }
    }
  }

  return !empty($postMeta) ? $content . $postMeta : $content;
}

function return_template_part($part, $param)
{
  ob_start();
  get_template_part($part, $param);
  return ob_get_clean();
}
