<?php
/**
 * The template for displaying the footer
 */
?>
      </div> <!-- .general__content -->
    </div> <!-- .container -->
  </section> <!-- .general -->

  <div class="footer container-wide">
    <?php
      wp_nav_menu(array(
        'container' => false,
        'theme_location' => 'footermenu',
        'menu_class'     => 'footer__nav'
      ));
    ?>
  </div>
<?php wp_footer(); ?>
</body>
</html>
