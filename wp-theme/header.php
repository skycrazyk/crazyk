<?php
/**
 * The template for displaying the header
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<?php if(is_singular() && pings_open(get_queried_object())) : ?>
	  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header class="header">
    <?php if(is_home()) : ?>
      <div class="logo" href="/"></div>
    <?php else : ?>
      <a class="logo" href="/"></a>
    <?php endif; ?>
  </header>

  <?php get_template_part('chunks/nav'); ?>

  <section class="general">
    <div class="container">
      <div class="general__content">
