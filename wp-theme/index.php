<?php
/**
 * The main template file
 */

get_header(); ?>

  <?php if ( have_posts() ) : ?>

    <?php
      while(have_posts()) : the_post();
        get_template_part('chunks/content', get_post_format());
      endwhile;
    ?>

    <div class="general__pagination">
      <?php
        the_posts_pagination(array(
          'prev_text'          => '<i class="icon-left"></i>',
          'next_text'          => '<i class="icon-right"></i>'
        ));
      ?>
    </div>
  <?php else :
    get_template_part( 'chunks/content', 'none' );
  endif; ?>

<?php get_footer(); ?>
